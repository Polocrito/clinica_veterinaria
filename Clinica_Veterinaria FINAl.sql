/* 
** CREABASE.SQL
**
DROP DATABASE Clinica_Veterinaria
*/

/* ********************************************************************
                        CREACION DE LA BASE DE DATOS
   ******************************************************************** */
   use master
   
CREATE DATABASE Clinica_Veterinaria  
  ON
  (NAME = Clinica_Veterinaria,           -- Primary data file
  FILENAME = 'E:\DB\Clinica_Veterinaria.mdf',
  SIZE = 5MB,
  MAXSIZE = 20MB,
  FILEGROWTH = 5MB
  )  
  LOG ON
  (NAME = Clinica_Veterinaria_Log,       -- Log file
  FILENAME = 'E:\DB\Clinica_Veterinaria.ldf',
  SIZE = 5MB,
  MAXSIZE = 20MB,
  FILEGROWTH = 2MB
  )
GO

/* ********************************************************************
                        CREACION DE TIPOS
   ******************************************************************** */
USE Clinica_Veterinaria   

EXEC  sp_addtype  TID_CLIENTE,         'varchar(6)', 'not null'
EXEC  sp_addtype  TID_MASCOTA,     'varchar(6)', 'not null'
EXEC  sp_addtype  TID_JAULA,       'varchar(5)', 'not null'
EXEC  sp_addtype  TID_Atencion,    'varchar(7)', 'not null'
EXEC  sp_addtype  TID_USUARIO,       'varchar(7)', 'not null'
EXEC  sp_addtype  TID_MEDICO,       'varchar(7)', 'not null'
EXEC  sp_addtype  TID_ADMIN,       'varchar(7)', 'not null'
EXEC  sp_addtype  TID_Secret,       'varchar(7)', 'not null'

GO


/* ********************************************************************
                        CREACION DE TABLAS
   ******************************************************************** */
CREATE TABLE Clientes (
ID_CLIENTE  TID_CLIENTE NOT NULL,
NOMBRE VARCHAR(30),
APELLIDO_P VARCHAR(30),
APELLIDO_M VARCHAR(30),
DIRECCION VARCHAR(1024),
CIUDAD VARCHAR(30),
PAIS VARCHAR(10),
TELEFONO INT,
CELULAR INT,
EMAIL VARCHAR(128),
PRIMARY KEY (ID_CLIENTE)
)
insert into Clientes values ('C001', 'Julian', 'Perez','Villalva','Sucre bajo','Cusco','Peru', '0846423','934555245','fater@gmail.com')

create table Mascota
 (
   ID_MASCOTA   TID_MASCOTA NOT NULL,
   ID_CLIENTE   TID_CLIENTE NOT NULL,
   NOMBRE        varchar(30),
   ESPECIE       varchar(30),
   RAZA          varchar(30),
   SEXO          varchar(30),   
    Primary key (ID_MASCOTA),
	Foreign key(ID_CLIENTE) references Clientes (ID_CLIENTE)	
  )
  insert into Mascota values ('M001','Manchas','Perro','Cooquer Spanish','Macho')

  CREATE TABLE Jaulas (
ID_JAULA     TID_JAULA NOT NULL,
ID_MASCOTA   TID_MASCOTA NOT NULL,
TIPO_JAULA VARCHAR(30),
ALTO FLOAT,
ANCHO FLOAT,
FONDO FLOAT,
PRIMARY KEY (ID_JAULA),
FOREIGN KEY (ID_MASCOTA) REFERENCES Mascota(ID_MASCOTA)
)

 create table Atencion
 (
   ID_Atencion    TID_Atencion NOT NULL,
   ID_MASCOTA      TID_MASCOTA NOT NULL,
   RESUMEN         varchar(50),
   FECHA           date,
   OBSERVACIONES   varchar(1024),
   Primary key ( ID_Atencion),
   Foreign key(ID_MASCOTA ) references Mascota(ID_MASCOTA )
 )

CREATE TABLE Usuarios (
ID_USUARIO TID_USUARIO NOT NULL,
PASWORD VARCHAR(32),
EMAIL VARCHAR(128),
PREG_SECRETA VARCHAR(256),
RESP_SECRETA VARCHAR(256),
NOMBRE VARCHAR(30),
APELLIDO_P VARCHAR(30),
APELLIDO_M VARCHAR(30),
PRIMARY KEY (ID_USUARIO)
)

CREATE TABLE Medico (
ID_MEDICO TID_MEDICO NOT NULL,
ID_USUARIO TID_USUARIO NOT NULL,
NOMBRE VARCHAR(30),
APELLIDO_P VARCHAR(30),
APELLIDO_M VARCHAR(30),
DIRECCION VARCHAR(1024),
CIUDAD VARCHAR(30),
PAIS VARCHAR(10),
CELULAR INT,
EMAIL VARCHAR(128),
PRIMARY KEY (ID_MEDICO),
Foreign key(ID_USUARIO ) references Usuarios(ID_USUARIO)
)

CREATE TABLE Administrador (
ID_ADMIN TID_ADMIN NOT NULL,
ID_USUARIO TID_USUARIO NOT NULL,
NOMBRE VARCHAR(30),
APELLIDO_P VARCHAR(30),
APELLIDO_M VARCHAR(30),
DIRECCION VARCHAR(1024),
CIUDAD VARCHAR(30),
PAIS VARCHAR(10),
CELULAR INT,
EMAIL VARCHAR(128),
PRIMARY KEY (ID_ADMIN),
Foreign key(ID_USUARIO ) references Usuarios(ID_USUARIO)
)

CREATE TABLE Secretaria (
ID_Secret TID_Secret NOT NULL,
ID_USUARIO TID_USUARIO NOT NULL,
NOMBRE VARCHAR(30),
APELLIDO_P VARCHAR(30),
APELLIDO_M VARCHAR(30),
DIRECCION VARCHAR(1024),
CIUDAD VARCHAR(30),
PAIS VARCHAR(10),
CELULAR INT,
EMAIL VARCHAR(128),
PRIMARY KEY (ID_Secret),
Foreign key(ID_USUARIO ) references Usuarios(ID_USUARIO)
)

GO

