using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Clases_Entidad;
using Clases_Interfaz;

namespace AppControl_Academico
{
	/// <summary>
	/// Summary description for fmMenu.
	/// </summary>
	public class fmMenu : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
        private System.Windows.Forms.MenuItem miCarreras;
        private IContainer components;

		public fmMenu()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.miCarreras = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem2});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem3,
            this.miCarreras});
            this.menuItem1.Text = "Mantenimientos";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 0;
            this.menuItem3.Text = "Mascotas";
            this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // miCarreras
            // 
            this.miCarreras.Index = 1;
            this.miCarreras.Text = "Clientes";
            this.miCarreras.Click += new System.EventHandler(this.miCarreras_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 1;
            this.menuItem2.Text = "Procesos";
            // 
            // fmMenu
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(392, 266);
            this.Menu = this.mainMenu1;
            this.Name = "fmMenu";
            this.Text = "CLINICA VETERINARIA";
            this.ResumeLayout(false);

		}
		#endregion

		[STAThread]
		static void Main() 
		{
			Application.Run(new fmMenu());
		}

		private void miCarreras_Click(object sender, System.EventArgs e)
		{		
		  fmCliente OCliente = new fmCliente();
		  OCliente.Show();		
		}

		private void menuItem3_Click(object sender, System.EventArgs e)
		{
			fmMascota OAlumno = new fmMascota();
			OAlumno.ShowDialog();		
		}

        private void menuItem1_Click(object sender, EventArgs e)
        {

        }
	}
}
