CREATE DATABASE DBControlAcademico   
GO

USE DBControlAcademico   

EXEC  sp_addtype  TCod_Carrera,    'varchar(2)', 'not null'
EXEC  sp_addtype  TCod_Alumno,     'varchar(6)', 'not null'
EXEC  sp_addtype  TCod_Docente,    'varchar(7)', 'not null'

GO

create table Carrera
  (
    Cod_Carrera         TCod_Carrera,
    Nom_Carrera         varchar(40)         null,
    Observaciones       varchar(40)         null,
    Primary key (Cod_Carrera)
  )

insert into Carrera values ('IN', 'Ing. Informática y de Sistemas', 'Ninguna')

create table Alumno
  (
    Cod_Alumno          TCod_Alumno,
    Paterno             Varchar(15)       not null,
    Materno             Varchar(15)       not null,
    Nombres             Varchar(15)       not null,
    Cod_Carrera         TCod_Carrera,
    Primary key (Cod_Alumno),
    Foreign key (Cod_Carrera) references Carrera(Cod_Carrera)
  )

create table Docente
  (
    Cod_Docente         TCod_Docente,
    Paterno             Varchar(15)       not null,
    Materno             Varchar(15)       not null,
    Nombres             Varchar(15)       not null,
    Categoria           Varchar(2)        default 'N'
                        Check (Categoria in ('PR','AS','AU','JP')),
    Regimen             Varchar(4)        default 'DE'
                        Check (Regimen in ('DE','TC','TP20','TP10')),
    Primary key (Cod_Docente)
  )

GO
