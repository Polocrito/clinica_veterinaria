using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Clases_Entidad;

namespace Clases_Interfaz
{
	public class fmCliente : fmMantenimiento
	{

		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TextBox tbCod_Carrera;
		private System.Windows.Forms.TextBox tbNombre;
		private System.Windows.Forms.TextBox tbPaterno;
		private System.Windows.Forms.Label laCod_Carrera;
		private System.Windows.Forms.Label laNom_Carrera;
		private System.Windows.Forms.Label laObservaciones;
        private Label label2;
        private TextBox tbDirec;
        private Label label1;
        private TextBox tbMaterno;
        private Label label7;
        private TextBox tbEmail;
        private Label label5;
        private TextBox tbCel;
        private Label label6;
        private TextBox tbTelef;
        private Label label4;
        private TextBox tbPais;
        private Label label3;
        private TextBox tbCiudad;
        private Button button4;
        private CCliente OCliente;


		public fmCliente() : base()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();            
			// TODO: Add any initialization after the InitializeComponent call
			/* Crear objeto Carrera */
            OCliente = new CCliente();
			/* Inicializar mantenimiento con este objeto */
			Inicializar(OCliente);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.tbCod_Carrera = new System.Windows.Forms.TextBox();
            this.tbNombre = new System.Windows.Forms.TextBox();
            this.tbPaterno = new System.Windows.Forms.TextBox();
            this.laCod_Carrera = new System.Windows.Forms.Label();
            this.laNom_Carrera = new System.Windows.Forms.Label();
            this.laObservaciones = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbMaterno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbDirec = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCiudad = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbPais = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbCel = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbTelef = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.paTitulo.SuspendLayout();
            this.paBotones.SuspendLayout();
            this.paDatos.SuspendLayout();
            this.SuspendLayout();
            // 
            // paTitulo
            // 
            this.paTitulo.Size = new System.Drawing.Size(616, 37);
            // 
            // laTitulo
            // 
            this.laTitulo.ForeColor = System.Drawing.Color.Yellow;
            this.laTitulo.Location = new System.Drawing.Point(217, 7);
            this.laTitulo.Size = new System.Drawing.Size(149, 27);
            this.laTitulo.Text = "CLIENTES";
            // 
            // paBotones
            // 
            this.paBotones.Controls.Add(this.button4);
            this.paBotones.Location = new System.Drawing.Point(0, 516);
            this.paBotones.Size = new System.Drawing.Size(616, 57);
            this.paBotones.Controls.SetChildIndex(this.buGrabar, 0);
            this.paBotones.Controls.SetChildIndex(this.buSalir, 0);
            this.paBotones.Controls.SetChildIndex(this.button4, 0);
            // 
            // buSalir
            // 
            this.buSalir.Location = new System.Drawing.Point(429, 7);
            // 
            // buGrabar
            // 
            this.buGrabar.Click += new System.EventHandler(this.buGrabar_Click);
            // 
            // paDatos
            // 
            this.paDatos.Controls.Add(this.label7);
            this.paDatos.Controls.Add(this.tbEmail);
            this.paDatos.Controls.Add(this.label5);
            this.paDatos.Controls.Add(this.tbCel);
            this.paDatos.Controls.Add(this.label6);
            this.paDatos.Controls.Add(this.tbTelef);
            this.paDatos.Controls.Add(this.label4);
            this.paDatos.Controls.Add(this.tbPais);
            this.paDatos.Controls.Add(this.label3);
            this.paDatos.Controls.Add(this.tbCiudad);
            this.paDatos.Controls.Add(this.label2);
            this.paDatos.Controls.Add(this.tbDirec);
            this.paDatos.Controls.Add(this.label1);
            this.paDatos.Controls.Add(this.tbMaterno);
            this.paDatos.Controls.Add(this.laObservaciones);
            this.paDatos.Controls.Add(this.laNom_Carrera);
            this.paDatos.Controls.Add(this.laCod_Carrera);
            this.paDatos.Controls.Add(this.tbPaterno);
            this.paDatos.Controls.Add(this.tbNombre);
            this.paDatos.Controls.Add(this.tbCod_Carrera);
            this.paDatos.Size = new System.Drawing.Size(616, 479);
            this.paDatos.Paint += new System.Windows.Forms.PaintEventHandler(this.paDatos_Paint);
            // 
            // tbCod_Carrera
            // 
            this.tbCod_Carrera.Location = new System.Drawing.Point(221, 16);
            this.tbCod_Carrera.Name = "tbCod_Carrera";
            this.tbCod_Carrera.Size = new System.Drawing.Size(127, 22);
            this.tbCod_Carrera.TabIndex = 0;
            this.tbCod_Carrera.Leave += new System.EventHandler(this.tbCod_Carrera_Leave);
            // 
            // tbNombre
            // 
            this.tbNombre.Location = new System.Drawing.Point(221, 61);
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.Size = new System.Drawing.Size(317, 22);
            this.tbNombre.TabIndex = 1;
            // 
            // tbPaterno
            // 
            this.tbPaterno.Location = new System.Drawing.Point(221, 102);
            this.tbPaterno.Name = "tbPaterno";
            this.tbPaterno.Size = new System.Drawing.Size(110, 22);
            this.tbPaterno.TabIndex = 2;
            // 
            // laCod_Carrera
            // 
            this.laCod_Carrera.Location = new System.Drawing.Point(86, 12);
            this.laCod_Carrera.Name = "laCod_Carrera";
            this.laCod_Carrera.Size = new System.Drawing.Size(120, 26);
            this.laCod_Carrera.TabIndex = 3;
            this.laCod_Carrera.Text = "DNI:";
            this.laCod_Carrera.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // laNom_Carrera
            // 
            this.laNom_Carrera.Location = new System.Drawing.Point(86, 61);
            this.laNom_Carrera.Name = "laNom_Carrera";
            this.laNom_Carrera.Size = new System.Drawing.Size(120, 27);
            this.laNom_Carrera.TabIndex = 4;
            this.laNom_Carrera.Text = "Nombre:";
            this.laNom_Carrera.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // laObservaciones
            // 
            this.laObservaciones.Location = new System.Drawing.Point(86, 102);
            this.laObservaciones.Name = "laObservaciones";
            this.laObservaciones.Size = new System.Drawing.Size(120, 27);
            this.laObservaciones.TabIndex = 5;
            this.laObservaciones.Text = "Apellido Paterno:";
            this.laObservaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(86, 149);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 27);
            this.label1.TabIndex = 7;
            this.label1.Text = "Apellido Materno:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbMaterno
            // 
            this.tbMaterno.Location = new System.Drawing.Point(221, 149);
            this.tbMaterno.Name = "tbMaterno";
            this.tbMaterno.Size = new System.Drawing.Size(110, 22);
            this.tbMaterno.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(86, 190);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 27);
            this.label2.TabIndex = 9;
            this.label2.Text = "Direccion:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbDirec
            // 
            this.tbDirec.Location = new System.Drawing.Point(221, 190);
            this.tbDirec.Name = "tbDirec";
            this.tbDirec.Size = new System.Drawing.Size(110, 22);
            this.tbDirec.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(86, 233);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 27);
            this.label3.TabIndex = 11;
            this.label3.Text = "Ciudad:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbCiudad
            // 
            this.tbCiudad.Location = new System.Drawing.Point(221, 233);
            this.tbCiudad.Name = "tbCiudad";
            this.tbCiudad.Size = new System.Drawing.Size(110, 22);
            this.tbCiudad.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(86, 270);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 27);
            this.label4.TabIndex = 13;
            this.label4.Text = "Pais:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbPais
            // 
            this.tbPais.Location = new System.Drawing.Point(221, 270);
            this.tbPais.Name = "tbPais";
            this.tbPais.Size = new System.Drawing.Size(110, 22);
            this.tbPais.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(86, 343);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 27);
            this.label5.TabIndex = 17;
            this.label5.Text = "Celular:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbCel
            // 
            this.tbCel.Location = new System.Drawing.Point(221, 343);
            this.tbCel.Name = "tbCel";
            this.tbCel.Size = new System.Drawing.Size(110, 22);
            this.tbCel.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(86, 306);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 27);
            this.label6.TabIndex = 15;
            this.label6.Text = "Telefono:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbTelef
            // 
            this.tbTelef.Location = new System.Drawing.Point(221, 306);
            this.tbTelef.Name = "tbTelef";
            this.tbTelef.Size = new System.Drawing.Size(110, 22);
            this.tbTelef.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(86, 380);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 27);
            this.label7.TabIndex = 19;
            this.label7.Text = "Email:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(221, 380);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(224, 22);
            this.tbEmail.TabIndex = 18;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(332, 9);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 23;
            this.button4.Text = "Eliminar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // fmCliente
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.ClientSize = new System.Drawing.Size(616, 573);
            this.Name = "fmCliente";
            this.Text = "Mantenimiento de CLIENTES";
            this.Load += new System.EventHandler(this.fmCarrera_Load);
            this.paTitulo.ResumeLayout(false);
            this.paBotones.ResumeLayout(false);
            this.paDatos.ResumeLayout(false);
            this.paDatos.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		#region M�todos Sobreescritos
		/* Los siguientes m�todos se sobre escriben, para adecuar este
		   formato, a las necesidades de un mantenimiento de Carreras */
		/* ************************************************* */
		public override void InicializarAtributoClave()
		{
			/* Inicializar atributo clave */
			tbCod_Carrera.Text = "";
			/* Poner foco en el codigo de carrera */
			tbCod_Carrera.Focus();			
		}
		/* ************************************************* */
		public override void InicializarAtributosNoClave()
		{
			/* Inicializar atributos */
			tbNombre.Text = "";
            tbPaterno.Text = "";
            tbMaterno.Text = "";
            tbDirec.Text = "";
            tbCiudad.Text = "";
            tbPais.Text = "";
            tbTelef.Text = "";
            tbCel.Text = "";
            tbEmail.Text = "";

        }
		/* ************************************************* */
		public override void RecuperarAtributos()
		{
           tbNombre.Text = OCliente.ValorAtributo("NOMBRE").ToString();
           tbPaterno.Text = OCliente.ValorAtributo("APELLIDO_P").ToString();
           tbMaterno.Text = OCliente.ValorAtributo("APELLIDO_M").ToString();
            tbDirec.Text = OCliente.ValorAtributo("DIRECCION").ToString();
            tbCiudad.Text = OCliente.ValorAtributo("CIUDAD").ToString();
            tbPais.Text = OCliente.ValorAtributo("PAIS").ToString();
            tbTelef.Text = OCliente.ValorAtributo("TELEFONO").ToString();
            tbCel.Text = OCliente.ValorAtributo("CELULAR").ToString();
            tbEmail.Text = OCliente.ValorAtributo("EMAIL").ToString();
        }
		/* ************************************************* */
		public override object[] AsignarAtributos()
		{
			Object [] Atributos = {tbCod_Carrera.Text,tbNombre.Text,tbPaterno.Text, tbMaterno.Text, tbDirec.Text
            ,tbCiudad.Text,tbPais.Text,tbTelef.Text,tbCel.Text,tbEmail.Text };
			return Atributos;
		}
		#endregion M�todos Sobreescritos

		/* ************************************************* */
		#region Eventos adicionales
		private void tbCod_Carrera_Leave(object sender, System.EventArgs e)
		{
			/* Convertir Clave a mayusculas */
			tbCod_Carrera.Text = tbCod_Carrera.Text.ToUpper();
			/* Procesar clave para determinar si es un registro nuevo o existente */
			ProcesarClave();
		}
        #endregion Eventos adicionales

        private void fmCarrera_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void paDatos_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buGrabar_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            /* Recuperar atributos, el primer atributo es la clave */
            Object[] Atributos = {tbCod_Carrera.Text,tbNombre.Text,tbPaterno.Text, tbMaterno.Text, tbDirec.Text
            ,tbCiudad.Text,tbPais.Text,tbTelef.Text,tbCel.Text,tbEmail.Text }; ;            // ----- Verificar si existe clave primaria 
            if (OCliente.ExisteClave(Atributos))
            {
                OCliente.Delete(Atributos);
                InicializarAtributoClave();
                InicializarAtributosNoClave();
            }
            else
                MessageBox.Show("El registro no existe");
            /* Inicializar un nuevo formato */
        }
    }
}

