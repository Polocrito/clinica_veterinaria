using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Clases_Entidad;

namespace Clases_Interfaz
{
	public class fmMascota : fmMantenimiento
	{
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label laCod_Alumno;
		private System.Windows.Forms.Label laNombres;
		private System.Windows.Forms.Label laCod_Carrera;
		private System.Windows.Forms.TextBox tbCod_Alumno;
		private System.Windows.Forms.TextBox tbRaza;
		private System.Windows.Forms.TextBox tbCod_Carrera;
		private System.ComponentModel.IContainer components = null;

		private CMascota OMascota;
		private System.Windows.Forms.TextBox tbNombre;
		private System.Windows.Forms.Label laPaterno;
		private System.Windows.Forms.TextBox tbEspecie;
		private System.Windows.Forms.Label laMaterno;
        private ComboBox cboCarrera;
        private TextBox tbFecNac;
        private Label label3;
        private TextBox tbSexo;
        private Label label2;
        private Button button4;
        private Button button3;
        private Button button2;
        private Button button1;
        private Label MSM;
        private CCliente OCliente; // ----- Para verificar Referencia de integridad

		public fmMascota() : base()
		{
			// This call is required by the Windows Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
			/* Crear objeto Carrera */
			OMascota = new CMascota();
			OCliente = new CCliente(); // ----- Para verificar referencias de integridad
			/* Inicializar mantenimiento con este objeto */
			Inicializar(OMascota);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.laCod_Alumno = new System.Windows.Forms.Label();
            this.laNombres = new System.Windows.Forms.Label();
            this.laCod_Carrera = new System.Windows.Forms.Label();
            this.tbCod_Alumno = new System.Windows.Forms.TextBox();
            this.tbRaza = new System.Windows.Forms.TextBox();
            this.tbCod_Carrera = new System.Windows.Forms.TextBox();
            this.tbNombre = new System.Windows.Forms.TextBox();
            this.laPaterno = new System.Windows.Forms.Label();
            this.tbEspecie = new System.Windows.Forms.TextBox();
            this.laMaterno = new System.Windows.Forms.Label();
            this.cboCarrera = new System.Windows.Forms.ComboBox();
            this.tbSexo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbFecNac = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.MSM = new System.Windows.Forms.Label();
            this.paTitulo.SuspendLayout();
            this.paBotones.SuspendLayout();
            this.paDatos.SuspendLayout();
            this.SuspendLayout();
            // 
            // paTitulo
            // 
            this.paTitulo.Size = new System.Drawing.Size(601, 37);
            // 
            // laTitulo
            // 
            this.laTitulo.Location = new System.Drawing.Point(159, 8);
            this.laTitulo.Size = new System.Drawing.Size(181, 26);
            this.laTitulo.Text = "MASCOTAS";
            // 
            // paBotones
            // 
            this.paBotones.Location = new System.Drawing.Point(0, 512);
            this.paBotones.Size = new System.Drawing.Size(601, 10);
            this.paBotones.Paint += new System.Windows.Forms.PaintEventHandler(this.paBotones_Paint);
            // 
            // buGrabar
            // 
            this.buGrabar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buGrabar.Location = new System.Drawing.Point(12, 4);
            this.buGrabar.Click += new System.EventHandler(this.buGrabar_Click);
            // 
            // paDatos
            // 
            this.paDatos.Controls.Add(this.MSM);
            this.paDatos.Controls.Add(this.button1);
            this.paDatos.Controls.Add(this.button4);
            this.paDatos.Controls.Add(this.button3);
            this.paDatos.Controls.Add(this.button2);
            this.paDatos.Controls.Add(this.tbFecNac);
            this.paDatos.Controls.Add(this.label3);
            this.paDatos.Controls.Add(this.tbSexo);
            this.paDatos.Controls.Add(this.label2);
            this.paDatos.Controls.Add(this.cboCarrera);
            this.paDatos.Controls.Add(this.tbEspecie);
            this.paDatos.Controls.Add(this.laMaterno);
            this.paDatos.Controls.Add(this.tbNombre);
            this.paDatos.Controls.Add(this.laPaterno);
            this.paDatos.Controls.Add(this.tbCod_Carrera);
            this.paDatos.Controls.Add(this.tbRaza);
            this.paDatos.Controls.Add(this.tbCod_Alumno);
            this.paDatos.Controls.Add(this.laCod_Carrera);
            this.paDatos.Controls.Add(this.laNombres);
            this.paDatos.Controls.Add(this.laCod_Alumno);
            this.paDatos.Size = new System.Drawing.Size(601, 475);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(208, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // laCod_Alumno
            // 
            this.laCod_Alumno.Location = new System.Drawing.Point(38, 37);
            this.laCod_Alumno.Name = "laCod_Alumno";
            this.laCod_Alumno.Size = new System.Drawing.Size(120, 26);
            this.laCod_Alumno.TabIndex = 0;
            this.laCod_Alumno.Text = "Cod Mascota:";
            this.laCod_Alumno.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // laNombres
            // 
            this.laNombres.Location = new System.Drawing.Point(38, 185);
            this.laNombres.Name = "laNombres";
            this.laNombres.Size = new System.Drawing.Size(120, 26);
            this.laNombres.TabIndex = 1;
            this.laNombres.Text = "Raza:";
            this.laNombres.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // laCod_Carrera
            // 
            this.laCod_Carrera.Location = new System.Drawing.Point(39, 316);
            this.laCod_Carrera.Name = "laCod_Carrera";
            this.laCod_Carrera.Size = new System.Drawing.Size(120, 26);
            this.laCod_Carrera.TabIndex = 2;
            this.laCod_Carrera.Text = "C�d. Cliente:";
            this.laCod_Carrera.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbCod_Alumno
            // 
            this.tbCod_Alumno.Location = new System.Drawing.Point(163, 37);
            this.tbCod_Alumno.Name = "tbCod_Alumno";
            this.tbCod_Alumno.Size = new System.Drawing.Size(106, 22);
            this.tbCod_Alumno.TabIndex = 0;
            this.tbCod_Alumno.TextChanged += new System.EventHandler(this.tbCod_Alumno_TextChanged);
            this.tbCod_Alumno.Leave += new System.EventHandler(this.tbCod_Alumno_Leave);
            // 
            // tbRaza
            // 
            this.tbRaza.Location = new System.Drawing.Point(163, 185);
            this.tbRaza.Name = "tbRaza";
            this.tbRaza.Size = new System.Drawing.Size(210, 22);
            this.tbRaza.TabIndex = 4;
            // 
            // tbCod_Carrera
            // 
            this.tbCod_Carrera.Location = new System.Drawing.Point(164, 316);
            this.tbCod_Carrera.Name = "tbCod_Carrera";
            this.tbCod_Carrera.Size = new System.Drawing.Size(39, 22);
            this.tbCod_Carrera.TabIndex = 5;
            this.tbCod_Carrera.Leave += new System.EventHandler(this.tbCod_Carrera_Leave);
            // 
            // tbNombre
            // 
            this.tbNombre.Location = new System.Drawing.Point(163, 83);
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.Size = new System.Drawing.Size(176, 22);
            this.tbNombre.TabIndex = 1;
            // 
            // laPaterno
            // 
            this.laPaterno.Location = new System.Drawing.Point(38, 83);
            this.laPaterno.Name = "laPaterno";
            this.laPaterno.Size = new System.Drawing.Size(120, 27);
            this.laPaterno.TabIndex = 6;
            this.laPaterno.Text = "Nombre:";
            this.laPaterno.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbEspecie
            // 
            this.tbEspecie.Location = new System.Drawing.Point(163, 129);
            this.tbEspecie.Name = "tbEspecie";
            this.tbEspecie.Size = new System.Drawing.Size(195, 22);
            this.tbEspecie.TabIndex = 2;
            // 
            // laMaterno
            // 
            this.laMaterno.Location = new System.Drawing.Point(38, 129);
            this.laMaterno.Name = "laMaterno";
            this.laMaterno.Size = new System.Drawing.Size(120, 27);
            this.laMaterno.TabIndex = 8;
            this.laMaterno.Text = "Especie:";
            this.laMaterno.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboCarrera
            // 
            this.cboCarrera.FormattingEnabled = true;
            this.cboCarrera.Location = new System.Drawing.Point(210, 315);
            this.cboCarrera.Name = "cboCarrera";
            this.cboCarrera.Size = new System.Drawing.Size(253, 24);
            this.cboCarrera.TabIndex = 9;
            // 
            // tbSexo
            // 
            this.tbSexo.Location = new System.Drawing.Point(163, 223);
            this.tbSexo.Name = "tbSexo";
            this.tbSexo.Size = new System.Drawing.Size(210, 22);
            this.tbSexo.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(38, 223);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 26);
            this.label2.TabIndex = 10;
            this.label2.Text = "Sexo:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbFecNac
            // 
            this.tbFecNac.Location = new System.Drawing.Point(164, 260);
            this.tbFecNac.Name = "tbFecNac";
            this.tbFecNac.Size = new System.Drawing.Size(210, 22);
            this.tbFecNac.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(-6, 256);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 26);
            this.label3.TabIndex = 12;
            this.label3.Text = "Fecha Nacimiento:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(55, 390);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Grabar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.TextChanged += new System.EventHandler(this.button2_TextChanged);
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(183, 390);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 16;
            this.button3.Text = "Nuevo";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(499, 445);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 17;
            this.button4.Text = "Salir";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(315, 390);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 18;
            this.button1.Text = "Eliminar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MSM
            // 
            this.MSM.AutoSize = true;
            this.MSM.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MSM.Location = new System.Drawing.Point(295, 37);
            this.MSM.Name = "MSM";
            this.MSM.Size = new System.Drawing.Size(0, 29);
            this.MSM.TabIndex = 19;
            // 
            // fmMascota
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.ClientSize = new System.Drawing.Size(601, 522);
            this.Name = "fmMascota";
            this.Text = "Mantenimiento de MASCOTAS";
            this.Load += new System.EventHandler(this.fmAlumno_Load);
            this.paTitulo.ResumeLayout(false);
            this.paBotones.ResumeLayout(false);
            this.paDatos.ResumeLayout(false);
            this.paDatos.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		#region M�todos Sobreescritos
		/* Los siguientes m�todos se sobre escriben, para adecuar este
		   formato, a las necesidades de un mantenimiento de Alumnos */
		/* ************************************************* */
		public override void InicializarAtributoClave()
		{
			/* Inicializar atributo clave */
			tbCod_Alumno.Text = "";
			/* Poner foco en el codigo de carrera */
			tbCod_Alumno.Focus();			
		}
		/* ************************************************* */
		public override void InicializarAtributosNoClave()
		{
			/* Inicializar atributos */
			tbNombre.Text = "";
			tbEspecie.Text = "";
			tbRaza.Text = "";
            tbSexo.Text = "";
            tbFecNac.Text = "";
            tbCod_Carrera.Text = "";			
		}
		/* ************************************************* */
		public override void RecuperarAtributos()
		{
			tbNombre.Text = OMascota.ValorAtributo("NOMBRE").ToString();
			tbEspecie.Text = OMascota.ValorAtributo("ESPECIE").ToString();
			tbRaza.Text = OMascota.ValorAtributo("RAZA").ToString();
            tbSexo.Text = OMascota.ValorAtributo("SEXO").ToString();
            tbFecNac.Text = OMascota.ValorAtributo("F_NAC").ToString();
            tbCod_Carrera.Text = OMascota.ValorAtributo("ID_CLIENTE").ToString();
		}
		/* ************************************************* */
		public override object[] AsignarAtributos()
		{
			object [] Atributos = {tbCod_Alumno.Text,
								   tbNombre.Text,
								   tbEspecie.Text,
								   tbRaza.Text,tbSexo.Text,tbFecNac.Text,
								   tbCod_Carrera.Text};
			return Atributos;
		}
		#endregion M�todos Sobreescritos

		/* ************************************************* */
		#region Eventos adicionales
		private void tbCod_Alumno_Leave(object sender, System.EventArgs e)
		{
			/* Convertir Clave a mayusculas */
			tbCod_Alumno.Text = tbCod_Alumno.Text.ToUpper();
            /* Procesar clave para determinar si es un registro nuevo o existente */
            //ProcesarClave();		
            /* Recuperar atributos, el primer atributo es la clave */
            object[] Atributos = AsignarAtributos();
            // ----- Verificar si existe clave primaria 
            if (OMascota.ExisteClave(Atributos))
            {
                /* Registro existente, Recuperar Atributos */
                RecuperarAtributos();
                MSM.Text = "Registro Existente";
            }
            else
            {
                /* Registro nuevo, inicializar atributos no clave */
                
                MSM.Text = "Registro Nuevo";
                tbCod_Carrera.Text = "";
                tbNombre.Text = "";
                tbEspecie.Text = "";
                tbRaza.Text = "";
                tbSexo.Text = "";
                tbFecNac.Text = "";
            }
        }

		private void tbCod_Carrera_Leave(object sender, System.EventArgs e)
		{
			/* Convertir Cod_Carrera a mayusculas */
			tbCod_Carrera.Text = tbCod_Carrera.Text.ToUpper();
			/* Verificar referencia de integridad */
			if (!OCliente.ExisteClave(tbCod_Carrera.Text))
			{
				MessageBox.Show("ERROR. C�digo de Cliente no existe en la Base de Datos...");
                tbCod_Carrera.Focus();
			}
            else
            {
                OCliente.RecuperarDatos("select * from Clientes where( '" + tbCod_Carrera.Text+ "'= ID_CLIENTE)");
                string Linea = OCliente.Datos.Tables[0].Rows[0]["NOMBRE"].ToString().ToUpper() + " " + OCliente.Datos.Tables[0].Rows[0]["APELLIDO_P"].ToString().ToUpper() + " " + OCliente.Datos.Tables[0].Rows[0]["APELLIDO_M"].ToString().ToUpper();
                cboCarrera.DisplayMember = "NOMBRE";
          
            }
		}
		#endregion Eventos adicionales

        private void fmAlumno_Load(object sender, EventArgs e)
        {
            OCliente.RecuperarDatos("select * from Clientes");

            for (int i = 0; i <= OCliente.Datos.Tables[0].Rows.Count - 1; i++)
            {
                string Linea = OCliente.Datos.Tables[0].Rows[i]["NOMBRE"].ToString().ToUpper() + " " + OCliente.Datos.Tables[0].Rows[i]["APELLIDO_P"].ToString().ToUpper()+" " + OCliente.Datos.Tables[0].Rows[i]["APELLIDO_M"].ToString().ToUpper();
                cboCarrera.Items.Add(Linea);
            }
            //cboCarrera.DataSource = OCliente.Datos.Tables[0];
            //cboCarrera.DisplayMember = "NOMBRE";
        }

        private void buGrabar_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tbCod_Alumno.Text = "";
            
            tbCod_Carrera.Text = "";
            tbNombre.Text = "";
            tbEspecie.Text = "";
            tbRaza.Text = "";
            tbSexo.Text = "";
            tbFecNac.Text = "";
        }

        private void paBotones_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            object[] Atributos = { tbCod_Alumno.Text, tbCod_Carrera.Text, tbNombre.Text, tbEspecie.Text, tbRaza.Text, tbSexo.Text, tbFecNac.Text };
            
            if (OMascota.ExisteClave(Atributos))
                OMascota.UpDate(Atributos);
            else
                {
                OMascota.Insert(Atributos);

            }
            tbCod_Alumno.Text = "";
            tbCod_Carrera.Text = "";
            tbNombre.Text = "";
            tbEspecie.Text = "";
            tbRaza.Text = "";
            tbSexo.Text = "";
            tbFecNac.Text = "";

        }

        private void button2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            /* Recuperar atributos, el primer atributo es la clave */
            object[] Atributos = { tbCod_Alumno.Text, tbCod_Carrera.Text, tbNombre.Text, tbEspecie.Text, tbRaza.Text, tbSexo.Text, tbFecNac.Text };
            // ----- Verificar si existe clave primaria 
            if (OMascota.ExisteClave(Atributos))
            {
                OMascota.Delete(Atributos);
                tbCod_Alumno.Text = "";
                tbCod_Carrera.Text = "";
                tbNombre.Text = "";
                tbEspecie.Text = "";
                tbRaza.Text = "";
                tbSexo.Text = "";
                tbFecNac.Text = "";
            }
            else
                MessageBox.Show("El registro no existe");
            /* Inicializar un nuevo formato */
            
        }

        private void tbCod_Alumno_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

