using System;

namespace Clases_Entidad
{
	public class CCarrera : CEntidad
	{
		/* CONSTRUCTORES */
		public CCarrera() : base("Carrera","Cod_Carrera")
		{

		}

		/* OTROS METODOS */
		/* ************************************************* */
		public override string ArgumentoSentenciaInsert(params object[] Atributos)
		{
			return "'"+Atributos[0]+"',"+ // ----- Cod_Carrera
				   "'"+Atributos[1]+"',"+ // ----- Nom_Carrera
				   "'"+Atributos[2]+"'";     // ----- Observaciones
		}

		/* ************************************************* */
		public override string ArgumentoSentenciaUpDate(params object[] Atributos)
		{
			return "Cod_Carrera = '"+Atributos[0]+"',"+ // ----- Cod_Carrera
				   "Nom_Carrera = '"+Atributos[1]+"',"+ // ----- Nom_Carrera
				   "Observaciones = '"+Atributos[2]+"'";     // ----- Observaciones
		}
	}
}
