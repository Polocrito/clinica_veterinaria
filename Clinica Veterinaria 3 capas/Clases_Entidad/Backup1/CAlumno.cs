using System;

namespace Clases_Entidad
{
	/// <summary>
	/// Summary description for CAlumno.
	/// </summary>
	public class CAlumno : CEntidad
	{
		/* CONSTRUCTORES */
		public CAlumno() : base("Alumno","Cod_Alumno")
	    {

	    }

		/* OTROS METODOS */
		/* ************************************************* */
		public override string ArgumentoSentenciaInsert(params object[] Atributos)
		{
			return "'"+Atributos[0]+"',"+ // ----- Cod_Alumno
		  		   "'"+Atributos[1]+"',"+ // ----- Paterno
		  		   "'"+Atributos[2]+"',"+ // ----- Materno
		  		   "'"+Atributos[3]+"',"+ // ----- Nombres
				   "'"+Atributos[4]+"'";  // ----- Cod_Carrera
		}

		/* ************************************************* */
		public override string ArgumentoSentenciaUpDate(params object[] Atributos)
		{
			return "Cod_Alumno = '"+Atributos[0]+"',"+ // ----- Cod_Alumno
				   "Paterno = '"+Atributos[1]+"',"+    // ----- Peterno
				   "Materno = '"+Atributos[2]+"',"+    // ----- Materno
				   "Nombres = '"+Atributos[3]+"',"+    // ----- Nombres
			  	   "Cod_Carrera = '"+Atributos[4]+"'"; // ----- Cod_Carrera
		}

	}
}
