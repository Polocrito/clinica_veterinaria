using System;

namespace Clases_Entidad
{
	public class CMascota : CEntidad
	{
		/* CONSTRUCTORES */
		public CMascota() : base("Mascota", "ID_MASCOTA")
		{

		}

		/* OTROS METODOS */
		/* ************************************************* */
		public override string ArgumentoSentenciaInsert(params object[] Atributos)
		{
			return "'"+Atributos[0]+"'," + // ----- ID_MASCOTA
                "'" + Atributos[1] + "'," + // ----- ID_CLIENTE
                   "'" +Atributos[2]+"',"+ // ----- NOMBRE
                   "'" + Atributos[3] + "'," + // ----- ESPECIE
                   "'" + Atributos[4] + "'," + // ----- RAZA
                   "'" + Atributos[5] + "'," + // ----- SEXO
                   "'" +Atributos[6]+"'";     // ----- F_NAC
		}

		/* ************************************************* */
		public override string ArgumentoSentenciaUpDate(params object[] Atributos)
		{
			return "ID_MASCOTA = '" + Atributos[0]+"',"+ // ----- ID_MASCOTA
                "ID_CLIENTE = '" + Atributos[1] + "'," + // ----- NOMBRE
                   "NOMBRE = '" + Atributos[2]+"',"+ // ----- NOMBRE
                    "ESPECIE = '" + Atributos[3] + "'," + // ----- ESPECIE
                     "RAZA = '" + Atributos[4] + "'," + // ----- RAZA
                      "SEXO = '" + Atributos[5] + "'," + // ----- SEXO
                   "F_NAC = '" +Atributos[6]+"'";     // ----- Observaciones
		}
	}
}
