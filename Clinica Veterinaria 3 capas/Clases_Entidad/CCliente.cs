using System;

namespace Clases_Entidad
{
	/// <summary>
	/// Summary description for CCliente.
	/// </summary>
	public class CCliente : CEntidad
	{
		/* CONSTRUCTORES */
		public CCliente() : base("Clientes", "ID_CLIENTE")
	    {

	    }

		/* OTROS METODOS */
		/* ************************************************* */
		public override string ArgumentoSentenciaInsert(params object[] Atributos)
		{
			return "'"+Atributos[0]+"'," + // ----- ID_CLIENTE
                   "'" +Atributos[1]+"',"+ // ----- NOMBRE
		  		   "'"+Atributos[2]+"'," + // ----- APELLIDO_P
                   "'" +Atributos[3]+"'," + // ----- APELLIDO_M
                   "'" +Atributos[4]+"',"+  // ----- DIRECCION
                   "'" + Atributos[5] + "'," +  // ----- CIUDAD
                   "'" + Atributos[6] + "'," +  // ----- PAIS
                   "'" + Atributos[7] + "'," +  // ----- TELEFONO
                   "'" + Atributos[8] + "'," +  // ----- CELULAR
                   "'" + Atributos[9] + "'";    //------EMAIL
        }

		/* ************************************************* */
		public override string ArgumentoSentenciaUpDate(params object[] Atributos)
		{
			return "ID_CLIENTE = '" + Atributos[0]+"'," + // ----- ID_CLIENTE
                   "NOMBRE = '" + Atributos[1]+"'," +    // ----- NOMBRE
                   "APELLIDO_P = '" + Atributos[2]+"'," +    // ----- APELLIDO_P
                   "APELLIDO_M = '" + Atributos[3]+"'," +    // ----- APELLIDO_P
                   "DIRECCION = '" + Atributos[4]+"',"+ // ----- DIRECCION
                   "CIUDAD = '" + Atributos[5] + "'," + // ----- CIUDAD
                   "PAIS = '" + Atributos[6] + "'," + // ----- PAIS
                   "TELEFONO = '" + Atributos[7] + "'," + // ----- TELEFONO
                   "CELULAR = '" + Atributos[8] + "'," + // ----- CELULAR
                   "EMAIL = '" + Atributos[9] + "'"; // ----- EMAIL
        }

	}
}
